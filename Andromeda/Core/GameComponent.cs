﻿namespace Andromeda.Core
{
    public abstract class GameComponent : IUpdatable, IDrawable
    {
        public abstract void Draw();
        public abstract void Update();
    }

    public interface IUpdatable
    {
        void Update();
    }

    public interface IDrawable
    {
        void Draw();
    }
}
