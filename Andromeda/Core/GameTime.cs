﻿using Andromeda.Base;
using System;
using System.Diagnostics;

namespace Andromeda.Core
{
    public class GameTime
    {
        public GameTime()
        {
            watch = new Stopwatch();
            watch.Start();
            recentTicks = watch.ElapsedTicks;
        }

        public void Initialize()
        {
            watch.Restart();
            recentTicks = watch.ElapsedTicks;
        }

        public void Update()
        {
            var ticks = watch.ElapsedTicks;
            var deltaTime = (float)(ticks - recentTicks) / Stopwatch.Frequency;
            if (isRecoverBreakPoint)
            {
                if (deltaTime > breakPointThreshold)
                    deltaTime = breakPointThreshold;
            }
            deltaTime = Second.Clamp(deltaTime, MinDeltaTime, MaxDeltaTime);
            this.deltaTime = (Second)deltaTime;
            recentTicks = ticks;
        }

        Second deltaTime;
        public Second DeltaTime
        {
            get
            {
                return deltaTime;
            }
        }

        /// <summary>
        /// 중단점으로 인해 실행이 멈춘 경우, 너무 큰 값이 deltaTime으로 넘어가지 않도록 하기 위한 안전 장치
        /// </summary>
        public void SetBreakPointSafeValue(Second threshold, Second deltaTime)
        {
            isRecoverBreakPoint = true;
            breakPointThreshold = threshold;
            breakPointDeltaTime = deltaTime;
        }

        public void UnsetBreakPointSafeValue()
        {
            isRecoverBreakPoint = false;
        }

        public Second MinDeltaTime = (Second)(1.0f / Stopwatch.Frequency);
        public Second MaxDeltaTime = (Second)float.MaxValue;

        Stopwatch watch;

        bool isRecoverBreakPoint;
        Second breakPointThreshold;
        Second breakPointDeltaTime;
        long recentTicks;
    }
}
