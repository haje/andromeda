﻿using OpenTK.Graphics.OpenGL;
using System;

namespace Andromeda.Core.Graphics
{
    public class VertexArray : IDisposable
    {
        private VertexArray(int vao, VertexBuffer vertBuffer, ElementBuffer elemBuffer)
        {
            this.vao = vao;
            this.vertBuffer = vertBuffer;
            this.elemBuffer = elemBuffer;
        }

        public static VertexArray CreateArray<T>(T[] vertices, VertexBuffer.Type usageType, int[] elements = null) where T : struct
        {
            var vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            var vertBuffer = VertexBuffer.Create(vertices, usageType);
            var elemBuffer = ElementBuffer.Create(elements);

            return new VertexArray(vao, vertBuffer, elemBuffer);
        }

        public void Bind()
        {
            GL.BindVertexArray(vao);
        }

        public void Draw()
        {
            Bind();
            GL.DrawElements(PrimitiveType.Triangles, ElementCount, DrawElementsType.UnsignedInt, 0);
        }

        public void UpdateVertex<T>(T[] vertices) where T : struct
        {
            Bind();
            vertBuffer.UpdateVertex(vertices);
        }

        public void Dispose()
        {
            if (vao != 0) GL.DeleteVertexArray(vao); vao = 0;
            if (vertBuffer != null) vertBuffer.Dispose(); vertBuffer = null;
            if (elemBuffer != null) elemBuffer.Dispose(); elemBuffer = null;
        }

        public int VertexCount
        {
            get
            {
                return vertBuffer.Count;
            }
        }

        public int ElementCount
        {
            get
            {
                return elemBuffer.Count;
            }
        }

        #region privates

        int vao;
        VertexBuffer vertBuffer;
        ElementBuffer elemBuffer;

        #endregion
    }
}
