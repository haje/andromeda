﻿using System;
using System.Collections.Generic;

namespace Andromeda.Core.Graphics
{
    public static class Shaders
    {
        internal static void Initialize(List<IDisposable> disposeList)
        {
            disposeList.Add(SpriteShader = new SpriteShader());
        }

        public static SpriteShader SpriteShader { get; private set; }
    }
}
