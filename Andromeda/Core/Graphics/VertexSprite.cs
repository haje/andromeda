﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Andromeda.Core.Graphics
{
    public struct VertexSprite
    {
        public Vector2 Position;
        public Vector2 TextureUV;

        public static void Setup()
        {
            int stride = BlittableValueType<VertexSprite>.Stride;

            int posAttr = 0;
            if (posAttr >= 0)
            {
                GL.EnableVertexAttribArray(posAttr);
                GL.VertexAttribPointer(posAttr, 2, VertexAttribPointerType.Float, false, stride, 0);
            }

            int UVAttr = 1;
            if (UVAttr >= 0)
            {
                GL.EnableVertexAttribArray(UVAttr);
                GL.VertexAttribPointer(UVAttr, 2, VertexAttribPointerType.Float, false, stride, 2 * sizeof(float));
            }

            GLHelper.CheckGLError();
        }
    }
}
