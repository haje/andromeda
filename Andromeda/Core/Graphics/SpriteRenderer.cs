﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;

namespace Andromeda.Core.Graphics
{
    public class SpriteRenderer : IDisposable
    {
        VertexSprite[] quad = new VertexSprite[4];
        VertexArray vertexArray;

        public SpriteRenderer()
        {
            int[] indices = new int[]
            {
               0, 1, 2,
               1, 2, 3
            };

            quad[0].TextureUV = new Vector2(0, 1);
            quad[0].Position = new Vector2(0, 0);

            quad[1].TextureUV = new Vector2(1, 1);
            quad[1].Position = new Vector2(1, 0);

            quad[2].TextureUV = new Vector2(0, 0);
            quad[2].Position = new Vector2(0, 1);

            quad[3].TextureUV = new Vector2(1, 0);
            quad[3].Position = new Vector2(1, 1);

            vertexArray = VertexArray.CreateArray
            (
                quad,
                VertexBuffer.Type.Static,
                indices
            );
        }

        public void Dispose()
        {
            if (vertexArray != null) vertexArray.Dispose(); vertexArray = null;
        }

        public void SetAlphaBlendMode()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }

        public void DrawLabel(RectangleF area, Texture labelTexture, ref Matrix3 transform, Color4 tint, Vector2 uvMin, Vector2 uvMax)
        {
            Matrix3 translMat = Matrix3.Identity;
            translMat.M31 = area.Location.X;
            translMat.M32 = area.Location.Y;
            Matrix3 scaleMat = Matrix3.CreateScale(area.Width, area.Height, 1.0f);
            Matrix3 mat = Matrix3.Mult(scaleMat, translMat);
            mat = Matrix3.Mult(mat, transform);

            //ShaderProvider.Label.Use(ref mat, GameSystem.Viewport, tint, labelTexture);
            vertexArray.Draw();
        }

        public void DrawTexture(RectangleF area, Texture texture, ref Matrix3 transform, Color4 tint, Vector2 uvMin, Vector2 uvMax)
        {
            Matrix3 translMat = Matrix3.Identity;
            translMat.M31 = area.Location.X;
            translMat.M32 = area.Location.Y;
            Matrix3 scaleMat = Matrix3.CreateScale(area.Width, area.Height, 1.0f);
            Matrix3 mat = Matrix3.Mult(scaleMat, translMat);
            mat = Matrix3.Mult(mat, transform);

            Shaders.SpriteShader.Use(ref mat, GameSystem.Viewport, tint, texture, uvMin, uvMax);
            vertexArray.Draw();
        }

        public void DrawTexture(Vector2 position, Texture texture)
        {
            Matrix3 transform = Matrix3.Identity;
            RectangleF area = new RectangleF(position.X, position.Y, texture.Size.X, texture.Size.Y);
            DrawTexture(area, texture, ref transform, Color4.White, Vector2.Zero, Vector2.One);
        }
    }
}
