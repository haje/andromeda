﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Andromeda.Core.Graphics
{
    public class Texture : IDisposable
    {
        public enum Types
        {
            Depth,
            Color
        }

        private Texture(int texture, Vector2 size, Types type = Types.Color)
        {
            this.texture = texture;
            this.size = size;
            this.type = type;
        }

        public static Texture CreateFromFile(string path, TextureWrapMode wrapMode = TextureWrapMode.ClampToBorder)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("텍스쳐 파일을 찾을 수 없습니다.\npath=" + path);

            return CreateFromStream(File.OpenRead(path), wrapMode);
        }

        public static Texture CreateFromStream(Stream stream, TextureWrapMode wrapMode = TextureWrapMode.ClampToBorder)
        {
            try
            {
                using (Bitmap bitmap = new Bitmap(stream))
                {
                    return CreateFromBitmap(bitmap, wrapMode);
                }
            }
            catch (ArgumentException e)
            {
                // Does not support some image extensions; e.g.) targa
                throw e;
            }
        }

        public static Texture CreateFromBitmap(Bitmap bitmap, TextureWrapMode wrapMode = TextureWrapMode.ClampToBorder)
        {
            var texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)wrapMode);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)wrapMode);

            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
            ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);
            bitmap.UnlockBits(bitmapData);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            return new Texture(texture, new Vector2(bitmap.Width, bitmap.Height));
        }

        public static Texture CreateEmptyBuffer(int width, int height, PixelInternalFormat pixelInternalFormat, OpenTK.Graphics.OpenGL.PixelFormat pixelFormat, PixelType pixelType, int minFilter = (int)TextureMinFilter.Linear, int magFilter = (int)TextureMagFilter.Linear, int wrap = (int)TextureWrapMode.ClampToBorder)
        {
            var texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, texture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, pixelInternalFormat, width, height, 0, pixelFormat, pixelType, IntPtr.Zero);
            Types type = Types.Color;

            if (pixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.DepthComponent)
                type = Types.Depth;

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, minFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, magFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, wrap);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, wrap);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            GLHelper.CheckGLError();

            return new Texture(texture, new Vector2(width, height), type);
        }

        public void Dispose()
        {
            if (texture != 0) GL.DeleteTexture(texture); texture = 0;
        }

        public Vector2 Size
        {
            get
            {
                return size;
            }
        }

        public int TextureHandle
        {
            get
            {
                return texture;
            }
        }

        public Types Type
        {
            get
            {
                return type;
            }
        }

        #region privates

        int texture;
        Vector2 size;
        Types type;

        #endregion
    }
}
