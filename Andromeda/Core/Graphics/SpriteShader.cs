﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Andromeda.Core.Graphics
{
    public class SpriteShader : ShaderBase
    {
        public SpriteShader()
            : base(vertexShader, fragmentShader)
        {
        }

        protected override void GetUniformLocation()
        {
            transfMatLoc = GL.GetUniformLocation(ShaderProgram, "transfMat");
            viewportLoc = GL.GetUniformLocation(ShaderProgram, "viewport");
            colorLoc = GL.GetUniformLocation(ShaderProgram, "color");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "tex");
            uvMinLoc = GL.GetUniformLocation(ShaderProgram, "uvMin");
            uvMaxLoc = GL.GetUniformLocation(ShaderProgram, "uvMax");
        }

        public void Use(ref Matrix3 transform, Vector2 viewport, Color4 color, Texture texture, Vector2 uvMin, Vector2 uvMax)
        {
            Use();
            GL.UniformMatrix3(transfMatLoc, false, ref transform);
            GL.Uniform2(viewportLoc, ref viewport);
            GL.Uniform4(colorLoc, color);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
            GL.Uniform1(textureLoc, 0);
            GL.Uniform2(uvMinLoc, uvMin);
            GL.Uniform2(uvMaxLoc, uvMax);
        }

        const string vertexShader = @"
#version 330 core
                
uniform mat3 transfMat;
uniform vec2 viewport;
uniform vec4 color;
uniform vec2 uvMin;
uniform vec2 uvMax;

layout (location = 0) in vec2 positionIn;
layout (location = 1) in vec2 textureUVIn;
                
out vec4 fragColor;
out vec2 texCoord;
                
void main()
{
    fragColor = color;
    texCoord = clamp((uvMax - uvMin) * textureUVIn + uvMin, vec2(0, 0), vec2(1, 1));
    vec3 transformed = transfMat * vec3(positionIn, 1);
    transformed.x = transformed.x / viewport.x * 2 - 1.0f;
    transformed.y = transformed.y / viewport.y * 2 - 1.0f;
    gl_Position = vec4(transformed, 1);
}
";
        const string fragmentShader = @"
#version 330 core

uniform sampler2D tex;

in vec4 fragColor;
in vec2 texCoord;

out vec4 colorOut;

void main()
{
    colorOut = texture(tex,texCoord) * fragColor;
}
";

        int transfMatLoc;
        int viewportLoc;
        int textureLoc;
        int colorLoc;
        int uvMinLoc;
        int uvMaxLoc;
    }
}
