﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace Andromeda.Core.Graphics
{
    public class VertexBuffer : IDisposable
    {
        public enum Type
        {
            Static,
            Dynamic
        }

        static BufferUsageHint HintFromType(Type type)
        {
            if (type == Type.Static)
                return BufferUsageHint.StaticDraw;
            else if (type == Type.Dynamic)
                return BufferUsageHint.DynamicDraw;
            throw new Exception("알 수 없는 VertexBufferType입니다.");
        }

        private VertexBuffer(int vbo, int count, BufferUsageHint hint)
        {
            this.vbo = vbo;
            this.count = count;
            this.hint = hint;
        }

        public static VertexBuffer Create<T>(T[] vertices, Type type) where T : struct
        {
            var count = vertices.Length;
            var stride = BlittableValueType.StrideOf(vertices);
            var vbo = GL.GenBuffer();
            var bufferHint = HintFromType(type);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), vertices, bufferHint);

            CheckUploadState(count, stride);
            VertexType.SetupFunctionOf(vertices)();
            GLHelper.CheckGLError();

            return new VertexBuffer(vbo, count, bufferHint);
        }

        public void UpdateVertex<T>(T[] vertices) where T : struct
        {
            Debug.Assert(hint == BufferUsageHint.DynamicDraw);
            Debug.Assert(vertices.Length <= count);

            var stride = BlittableValueType.StrideOf(vertices);

            Bind();
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * stride), vertices, hint);
        }

        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
        }

        public void Dispose()
        {
            if (vbo != 0) GL.DeleteBuffer(vbo); vbo = 0;
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        #region privates

        [Conditional("DEBUG")]
        static void CheckUploadState(int count, int stride)
        {
            int size;
            GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * stride != size)
                throw new ApplicationException("Vertex data not uploaded correctly");
        }

        int vbo;
        int count;
        BufferUsageHint hint;

        #endregion
    }
}
