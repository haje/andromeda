﻿using OpenTK.Graphics.OpenGL;
using System;

namespace Andromeda.Core.Graphics
{
    public class ElementBuffer : IDisposable
    {
        private ElementBuffer(int ebo, int count)
        {
            this.ebo = ebo;
            this.count = count;
        }

        public static ElementBuffer Create(int[] elements)
        {
            if (elements != null)
            {
                var count = elements.Length;
                var ebo = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(int) * count), elements, BufferUsageHint.StaticDraw);

                int size;
                GL.GetBufferParameter(BufferTarget.ElementArrayBuffer, BufferParameterName.BufferSize, out size);
                if (count * sizeof(int) != size)
                    throw new ApplicationException("Element data not uploaded correctly");

                return new ElementBuffer(ebo, count);
            }
            else
            {
                return null;
            }
        }

        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
        }

        public void Dispose()
        {
            if (ebo != 0) GL.DeleteBuffer(ebo); ebo = 0;
        }

        public static void Unbind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        #region privates

        int ebo;
        int count;

        #endregion
    }
}
