﻿using OpenTK.Graphics.OpenGL;
using System.Diagnostics;

namespace Andromeda.Core.Graphics
{
    public static class GLHelper
    {
        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            var error = GL.GetError();
            Debug.Assert(error == ErrorCode.NoError || error == ErrorCode.InvalidEnum);
        }
    }
}
