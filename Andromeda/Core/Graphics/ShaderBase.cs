﻿using OpenTK.Graphics.OpenGL;
using System;

namespace Andromeda.Core.Graphics
{
    public abstract class ShaderBase : IDisposable
    {
        public ShaderBase(string vSource, string fSource)
            : this(vSource, fSource, null)
        {
        }

        public ShaderBase(string vSource, string fSource, string gSource)
        {
            CreateVertexShader(vSource);
            CreateFragmentShader(fSource);
            CreateGeometryShader(gSource);
            LinkShader();
            GetUniformLocation();
        }

        #region private Helpers

        private int CompileShader(int shader, string source)
        {
            string errorInfo;
            int errorCode;

            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            GL.GetShaderInfoLog(shader, out errorInfo);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out errorCode);

            if (errorCode != 1)
                throw new ApplicationException(errorInfo);

            return shader;
        }

        void CreateVertexShader(string source)
        {
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            CompileShader(vertexShader, source);
        }

        void CreateGeometryShader(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                geometryShader = 0;
                return;
            }

            geometryShader = GL.CreateShader(ShaderType.GeometryShader);
            CompileShader(geometryShader, source);
        }

        void CreateFragmentShader(string source)
        {
            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            CompileShader(fragmentShader, source);
        }

        void LinkShader()
        {
            string errorInfo;
            int errorCode;

            ShaderProgram = GL.CreateProgram();
            GL.AttachShader(ShaderProgram, vertexShader);
            GL.AttachShader(ShaderProgram, fragmentShader);
            if (geometryShader != 0)
                GL.AttachShader(ShaderProgram, geometryShader);
            BindFragDataLocation();
            GL.LinkProgram(ShaderProgram);

            GL.GetProgramInfoLog(ShaderProgram, out errorInfo);
            GL.GetProgram(ShaderProgram, GetProgramParameterName.LinkStatus, out errorCode);
            if (errorCode != 1)
                throw new ApplicationException(errorInfo);
        }

        #endregion

        protected abstract void GetUniformLocation();

        protected virtual void BindFragDataLocation()
        {
            GL.BindFragDataLocation(ShaderProgram, 0, "colorOut");
        }

        public void Use()
        {
            GL.UseProgram(ShaderProgram);
        }

        public void Dispose()
        {
            if (vertexShader != 0) GL.DeleteShader(vertexShader); vertexShader = 0;
            if (geometryShader != 0) GL.DeleteShader(geometryShader); geometryShader = 0;
            if (fragmentShader != 0) GL.DeleteShader(fragmentShader); fragmentShader = 0;
            if (ShaderProgram != 0) GL.DeleteProgram(ShaderProgram); ShaderProgram = 0;
        }

        int vertexShader;
        int geometryShader;
        int fragmentShader;
        public int ShaderProgram { get; protected set;  }
    }
}
