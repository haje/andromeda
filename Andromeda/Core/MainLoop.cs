﻿using Andromeda.Base;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Diagnostics;

namespace Andromeda.Core
{
    public static class MainLoop
    {
        internal static void Initialize(GameLoop loop)
        {
            loop.Initialize();
            loop.Begin();
            runningLoopHolder.RunningLoop = loop;
            GameTime.Initialize();
        }

        internal static void Update()
        {
            JoinReservedLoop();
            EnsureListNotDirty();

            GameTime.Update();
            foreach (var u in updateListCopy)
                u.Update();
        }

        internal static void Draw()
        {
            GL.ClearColor(ClearColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            EnsureListNotDirty();
            foreach (var d in drawListCopy)
                d.Draw();

            GameSystem.GameWindow.Window.SwapBuffers();
        }

        public static void AddComponent(GameComponent component)
        {
            updateList.Add(component);
            drawList.Add(component);
            listDirty = true;
        }

        public static void RemoveComponent(GameComponent component)
        {
            updateList.Remove(component);
            drawList.Remove(component);
            listDirty = true;
        }

        public static void SetUpdateList(params IUpdatable[] orderedUpdateList)
        {
            updateList.Clear();
            updateList.AddRange(orderedUpdateList);
            listDirty = true;
        }

        public static void SetDrawList(params IDrawable[] orderedDrawList)
        {
            drawList.Clear();
            drawList.AddRange(orderedDrawList);
            listDirty = true;
        }

        public static void SetGameLoop(GameLoop loop)
        {
            if (loop == null)
                loop = Core.GameLoop.CreateEmpty();
            reservedLoop = loop;
        }

        public static Color4 ClearColor = Color4.Black;

        public static bool VerticalSyncronization
        {
            get
            {
                return GameSystem.GameWindow.Window.VSync == OpenTK.VSyncMode.On;
            }
            set
            {
                if (value == true)
                    GameSystem.GameWindow.Window.VSync = OpenTK.VSyncMode.On;
                else
                    GameSystem.GameWindow.Window.VSync = OpenTK.VSyncMode.Off;
            }
        }

        public static GameComponent GameLoop
        {
            get
            {
                return runningLoopHolder;
            }
        }

        public static Second DeltaTime
        {
            get
            {
                return GameTime.DeltaTime;
            }
        }

        public static readonly GameTime GameTime;

        #region privates

        static List<IUpdatable> updateList;
        static List<IDrawable> drawList;

        static bool listDirty = true;
        static List<IUpdatable> updateListCopy;
        static List<IDrawable> drawListCopy;

        static void EnsureListNotDirty()
        {
            if (!listDirty) return;
            updateListCopy = new List<IUpdatable>(updateList);
            drawListCopy = new List<IDrawable>(drawList);
            listDirty = false;
        }

        class RunningLoopHolder : GameComponent
        {
            public RunningLoopHolder()
            {
                RunningLoop = Core.GameLoop.CreateEmpty();
            }

            public GameLoop RunningLoop;

            public override void Draw()
            {
                RunningLoop.Draw();
            }

            public override void Update()
            {
                RunningLoop.Update();
            }
        }

        static RunningLoopHolder runningLoopHolder;
        static GameLoop reservedLoop;

        static void JoinReservedLoop()
        {
            if (reservedLoop != null)
            {
                runningLoopHolder.RunningLoop.End();
                runningLoopHolder.RunningLoop = reservedLoop;
                reservedLoop.Begin();
                reservedLoop = null;
            }
        }

        static MainLoop()
        {
            runningLoopHolder = new RunningLoopHolder();
            drawList = new List<IDrawable>();
            updateList = new List<IUpdatable>();
            AddComponent(runningLoopHolder);
            EnsureListNotDirty();

            GameTime = new GameTime();
            if (Debugger.IsAttached)
                GameTime.SetBreakPointSafeValue((Second)1.0f, (Second)0.1f);
        }

        #endregion
    }
}
