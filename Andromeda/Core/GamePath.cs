﻿using System;
using System.Diagnostics;
using System.IO;

namespace Andromeda.Core
{
    public class GamePath
    {
        public GamePath()
        {
            UpdateResourcePath();
        }

        string absolutePathToResource;
        string resourceDirectory = "Resource";
        public string ResourceDirectory
        {
            get
            {
                return absolutePathToResource;
            }
            set
            {
                resourceDirectory = value;
                UpdateResourcePath();
            }
        }

        static bool isResourceDirectoryProjectRelative = true;
        public bool IsResourceDirectoryProjectRelative
        {
            get
            {
                return isResourceDirectoryProjectRelative;
            }
            set
            {
                isResourceDirectoryProjectRelative = value;
                UpdateResourcePath();
            }
        }

        string relativePathFromBinaryToProject = "../../";
        public string RelativePathFromBinaryToProject
        {
            get
            {
                return relativePathFromBinaryToProject;
            }
            set
            {
                relativePathFromBinaryToProject = value;
                UpdateResourcePath();
            }
        }

        public readonly string BinaryDirectory = AppDomain.CurrentDomain.BaseDirectory;

        void UpdateResourcePath()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            absolutePathToResource = Path.GetFullPath(Path.Combine(dir, resourceDirectory)) + "\\";
            if (isResourceDirectoryProjectRelative)
            {
                var projectResource = Path.Combine(dir, relativePathFromBinaryToProject, resourceDirectory);
                if (Directory.Exists(projectResource))
                {
                    absolutePathToResource = Path.GetFullPath(projectResource) + "\\";
                }
            }
        }
    }
}
