﻿using Andromeda.Base;
using System;
using System.Diagnostics;

namespace Andromeda.Core
{
    public abstract class GameLoop : GameComponent, IDisposable
    {
        public GameLoop(string debugName)
        {
            DebugName = debugName;
            status = Status.Created;
        }

        public static GameLoop CreateEmpty()
        {
            return new HandlerWrapper(delegate { }, delegate { }, "EmptyLoop");
        }

        public static GameLoop CreateWithHandlers(GameLoopHandler updateHandler, GameLoopHandler drawHandler, string debugName)
        {
            return new HandlerWrapper(updateHandler, drawHandler, debugName);
        }

        public void Initialize()
        {
            Debug.Assert(status == Status.Created);
            OnInitialize();
            status = Status.Initialized;
        }

        protected virtual void OnInitialize()
        {

        }

        public void Begin()
        {
            Debug.Assert(status != Status.Disposed);
            if (status == Status.Running)
                return;

            if (status == Status.Created)
                Initialize();

            OnBegin();
            status = Status.Running;
        }

        protected virtual void OnBegin()
        {

        }

        public void End()
        {
            if (status != Status.Running)
                return;

            OnEnd();
            status = Status.Ended;
        }

        protected virtual void OnEnd()
        {

        }

        public void Dispose()
        {
            if (status != Status.Disposed)
                Dispose(true);

            status = Status.Disposed;
        }

        public virtual void Dispose(bool manual)
        {
            status = Status.Disposed;
        }

        public override string ToString()
        {
            return DebugName;
        }

        public readonly string DebugName;

        public enum Status
        {
            Created,
            Initialized,
            Running,
            Ended,
            Disposed
        }
        Status status;

        class HandlerWrapper : GameLoop
        {
            public HandlerWrapper(GameLoopHandler updateHandler,GameLoopHandler drawHandler, string debugName)
                : base(debugName)
            {
                this.drawHandler = drawHandler;
                this.updateHandler = updateHandler;
            }

            public override void Draw()
            {
                drawHandler();
            }

            public override void Update()
            {
                updateHandler();
            }

            GameLoopHandler drawHandler;
            GameLoopHandler updateHandler;
        }
    }

    public delegate void GameLoopHandler();
}
