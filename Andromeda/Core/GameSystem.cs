﻿using Andromeda.Core.Graphics;
using OpenTK;
using System;
using System.Collections.Generic;

namespace Andromeda.Core
{
    public static class GameSystem
    {
        public class Parameters
        {
            public string GameName = "Andromeda";
            public int ScreenWidth = 1024;
            public int ScreenHeight = 768;
            public bool FullScreen = false;
            public bool FixedWindow = true;
            public bool DebugMode = true;
            public bool VerticalSynchronization = false;

            public static readonly Parameters Default = new Parameters();
        }

        public static void Initialize()
        {
            Initialize(Parameters.Default);
        }

        public static void Initialize(string gameName, int screenWidth, int screenHeight)
        {
            var parameters = new Parameters();
            parameters.GameName = gameName;
            parameters.ScreenWidth = screenWidth;
            parameters.ScreenHeight = screenHeight;
            Initialize(parameters);
        }

        public static void Initialize(Parameters parameters)
        {
            Path = new GamePath();
            GameWindow = new GameWindow(parameters);
            Viewport = new Vector2(GameWindow.Window.Width, GameWindow.Window.Height);
            GameWindow.Window.Resize += delegate { Viewport = new Vector2(GameWindow.Window.Width, GameWindow.Window.Height); };
            disposeList.Add(SpriteRenderer = new SpriteRenderer());
            Shaders.Initialize(disposeList);
        }

        public static void Run(GameLoop gameLoop)
        {
            MainLoop.Initialize(gameLoop);
            var window = GameWindow.Window;
            window.UpdateFrame += delegate { MainLoop.Update(); };
            window.RenderFrame += delegate { MainLoop.Draw(); };
            window.Run();
        }

        public static void Exit()
        {
            GameWindow.Window.Close();

            disposeList.Reverse();
            foreach (var d in disposeList)
                d.Dispose();
            disposeList.Clear();
        }

        public static GameWindow GameWindow { get; private set; }

        public static SpriteRenderer SpriteRenderer { get; private set; }

        public static GamePath Path { get; private set; }

        public static Vector2 Viewport { get; private set;  }

        static List<IDisposable> disposeList = new List<IDisposable>();
    }
}
