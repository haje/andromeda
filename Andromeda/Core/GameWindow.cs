﻿using OpenTK;
using OpenTK.Graphics;

namespace Andromeda.Core
{
    public class GameWindow
    {
        internal GameWindow(GameSystem.Parameters parameters)
        {
            // windowFlag from parameters
            var windowFlag = GameWindowFlags.Default;
            if (parameters.FullScreen)
                windowFlag = GameWindowFlags.Fullscreen;
            else if (parameters.FixedWindow)
                windowFlag = GameWindowFlags.FixedWindow;

            // OpenGL 버전 정의
            var GLMajorVersion = 3;
            var GLMinorVersion = 2;

            // contextFlag from parameters
            var contextFlag = GraphicsContextFlags.ForwardCompatible;
            if (parameters.DebugMode)
                contextFlag = contextFlag | GraphicsContextFlags.Debug;

            // OpenTK 창 생성
            Window = new OpenTK.GameWindow(
                parameters.ScreenWidth,
                parameters.ScreenHeight,
                GraphicsMode.Default,
                parameters.GameName,
                windowFlag,
                DisplayDevice.Default,
                GLMajorVersion,
                GLMinorVersion,
                contextFlag);
        }

        public string Title
        {
            get
            {
                return Window.Title;
            }
            set
            {
                Window.Title = value;
            }
        }

        internal OpenTK.GameWindow Window;
    }
}
