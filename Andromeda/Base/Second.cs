﻿namespace Andromeda.Base
{
    /// <summary>
    /// 시간의 초 단위를 나타내는 타입.
    /// 시간 단위를 타입으로 명시해서 바로 알아볼 수 있게 한다.
    /// 
    /// number->Second는 명시적 변환
    /// Second->number암시적 변환이 된다.
    /// 
    /// 기타 연산은 float과 동일하게 사용 가능해야한다.
    /// </summary>
    public struct Second
    {
        private float value;

        public static Second Zero = new Second(0);
        public static Second Infinity = new Second(float.PositiveInfinity);

        #region .ctor

        private Second(float value)
        {
            this.value = value;
        }

        #endregion

        #region casting

        public static explicit operator Second(float time)
        {
            return new Second(time);
        }

        public static implicit operator float(Second time)
        {
            return time.value;
        }

        #endregion

        #region operator

        public static Second operator -(Second lhs, Second rhs)
        {
            return (Second)(lhs.value - rhs.value);
        }

        public static Second operator -(float lhs, Second rhs)
        {
            return (Second)(lhs - rhs.value);
        }

        public static Second operator -(Second lhs, float rhs)
        {
            return (Second)(lhs.value - rhs);
        }

        public static Second operator +(Second lhs, Second rhs)
        {
            return (Second)(lhs.value + rhs.value);
        }

        public static Second operator +(Second lhs, float rhs)
        {
            return (Second)(lhs.value + rhs);
        }

        public static Second operator +(float lhs, Second rhs)
        {
            return (Second)(lhs + rhs.value);
        }

        public static Second operator *(Second lhs, Second rhs)
        {
            return (Second)(lhs.value * rhs.value);
        }

        public static Second operator *(Second lhs, float rhs)
        {
            return (Second)(lhs.value * rhs);
        }

        public static Second operator *(float lhs, Second rhs)
        {
            return (Second)(lhs * rhs.value);
        }

        public static Second operator /(Second lhs, Second rhs)
        {
            return (Second)(lhs.value / rhs.value);
        }

        public static Second operator /(Second lhs, float rhs)
        {
            return (Second)(lhs.value / rhs);
        }

        public static Second operator /(float lhs, Second rhs)
        {
            return (Second)(lhs / rhs.value);
        }

        #endregion

        #region static methods

        public static Second Min(float lhs, float rhs)
        {
            if (lhs < rhs)
                return (Second)lhs;
            return (Second)rhs;
        }

        public static Second Max(float lhs, float rhs)
        {
            if (lhs > rhs)
                return (Second)lhs;
            return (Second)rhs;
        }

        public static Second Clamp(float value, float min, float max)
        {
            if (value < min)
                return (Second)min;
            if (value > max)
                return (Second)max;
            return (Second)value;
        }

        #endregion

        #region override

        public override string ToString()
        {
            return value + "seconds";
        }

        #endregion
    }
}
