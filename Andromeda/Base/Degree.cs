﻿
namespace Andromeda.Base
{
    public struct Degree
    {
        private float value;

        #region .ctor

        public Degree(float degree)
        {
            value = degree;
        }

        public Degree(Radian radian)
        {
            value = radian / Radian.Pi * 180.0f;
        }

        #endregion

        #region casting

        public static explicit operator Degree(float radian)
        {
            return new Degree(radian);
        }

        public static implicit operator Degree(Radian radian)
        {
            return new Degree(radian);
        }

        public static explicit operator float(Degree degree)
        {
            return degree.value;
        }

        #endregion

        #region operator

        public static Degree operator +(Degree a, Degree b)
        {
            return new Degree(a.value + b.value);
        }

        public static Degree operator +(Degree a, float b)
        {
            return new Degree(a.value + b);
        }

        public static Degree operator +(float a, Degree b)
        {
            return new Degree(a + b.value);
        }

        public static Degree operator -(Degree a, Degree b)
        {
            return new Degree(a.value - b.value);
        }

        public static Degree operator -(Degree a, float b)
        {
            return new Degree(a.value - b);
        }

        public static Degree operator -(float a, Degree b)
        {
            return new Degree(a - b.value);
        }

        public static Degree operator /(Degree a, Degree b)
        {
            return new Degree(a.value / b.value);
        }

        public static Degree operator /(Degree a, float b)
        {
            return new Degree(a.value / b);
        }

        public static Degree operator /(float a, Degree b)
        {
            return new Degree(a / b.value);
        }

        public static Degree operator *(Degree a, Degree b)
        {
            return new Degree(a.value * b.value);
        }

        public static Degree operator *(Degree a, float b)
        {
            return new Degree(a.value * b);
        }

        public static Degree operator *(float a, Degree b)
        {
            return new Degree(a * b.value);
        }

        #endregion
    }
}
