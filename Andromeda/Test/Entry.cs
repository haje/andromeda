﻿using Andromeda.Core;
using Andromeda.Core.Graphics;
using OpenTK;
using OpenTK.Graphics;

namespace Andromeda.Test
{
    static class Entry
    {
        static void Main()
        {
            GameSystem.Initialize("Andromeda Test", 1024, 768);
            var texture = Texture.CreateFromFile(GameSystem.Path.ResourceDirectory + "hello_andromeda.png");
            MainLoop.ClearColor = Color4.CornflowerBlue;
            MainLoop.SetUpdateList(
                MainLoop.GameLoop
            );
            MainLoop.SetDrawList(
                MainLoop.GameLoop
            );
            GameSystem.SpriteRenderer.SetAlphaBlendMode();
            GameSystem.Run(GameLoop.CreateWithHandlers(
                delegate { },
                delegate
                {
                    GameSystem.SpriteRenderer.DrawTexture(new Vector2(0, 0), texture);
                },
                "Andromeda.Test"));
            GameSystem.Exit();
        }
    }
}
